#!/bin/bash

# makes a backup of many critical files I have, to other drives.

DEST_DIR1='/titanium/backup/auto'
DEST_DIR2='/reserved/seagate/var/1/backup/auto'

DEST_DIR_BIG1='/titanium/backup/copy'
DEST_DIR_BIG2='/reserved/seagate/var/1/backup/copy'

function usage() {
	echo ""
	echo "This backs up critical files"
	echo "Usage:"
	echo ""
	echo "    backup <command>"
	echo ""
	echo "    Commands:"
	echo "       big: backs up all of /share"
	echo "              The big backup uses rsync to copy the entire /share directory"
	echo "       quick: backs up the /share/static/etc and some files in your home dir"
        echo "              The quick backup makes a bzip of config files"
	echo "       clean: removes some old bzip backups produced using the quick command"
	echo ""
}

function big_backup() {
	# this will actually rsync the entire /share drive
	rsync -avz /share/ $DEST_DIR_BIG1
	rsync -avz /share/ $DEST_DIR_BIG2
}

function quick_backup() {
        # this is the backup that should run frequently
        FILENAME=backup.quick.$(date +%Y-%m-%d_%H:%M).tar.bz
        tar jcvf $DEST_DIR1/$FILENAME /share/static/etc /home/michael/.bashrc /home/michael/.ssh
        cp $DEST_DIR1/$FILENAME $DEST_DIR2/$FILENAME
}


function clean() {
        find $DEST_DIR1 -name 'backup.quick.*' -ctime +7 -delete
}

function run() {
	CMD=$1
	if [ "big" == "$CMD" ]; then
		big_backup
	elif [ "quick" == "$CMD" ]; then
		quick_backup
	elif [ "clean" == "$CMD" ]; then
		clean
	else
		usage
	fi	
}

run $@
