
# use the profile, in case this is not automatically called
. /share/var/workspace/management/etc/profile.d/shared_profile.sh

umask 0002

export CLASSPATH='.:/share/usr/antlr/antlr-4.5-complete.jar'

export USR_DIR='/share/static/usr'
export WORKSPACE='/share/var/workspace'
export PIG_HOME="$USR_DIR/pig"
export MAVEN_HOME="$USR_DIR/maven"
export JAVA_HOME="$USR_DIR/java/jdk8"
export HIVE_HOME="$USR_DIR/hive"
export HADOOP_PREFIX="$USR_DIR/hadoop"
export HADOOP_HOME=$HADOOP_PREFIX
export HADOOP_COMMON_HOME=$HADOOP_PREFIX
export HADOOP_LOG_DIR="/share/var/log/hadoop"
export TARBALL_ROOT="/opt/cloudera-manager/cm"
export ACCUMULO_HOME="$USR_DIR/accumulo"
export ACCUMULO_CONF_DIR="$ACCUMULO_HOME/conf"
export ACCUMULO_LOG_DIR="/share/var/log/accumulo"
export HBASE_HOME="$USR_DIR/hbase"
export SPARK_HOME="$USR_DIR/spark"
export IMPALA_HOME="$USR_DIR/impala"
export MAHOUT_HOME="$USR_DIR/mahout"
export STORM_HOME="$USR_DIR/storm"
export NIFI_HOME="$USR_DIR/nifi"
export MONGO_HOME="$USR_DIR/mongo"
export SONAR_HOME="$USR_DIR/sonar"
export SCALA_HOME="$USR_DIR/scala"
export SQOOP_HOME="$USR_DIR/sqoop"
export ZOOKEEPER_HOME="$USR_DIR/zookeeper"
export QUICK_CODE_HOME='/share/usr/tools'
# storm, impala, mongo, sonar, scala

export PATH="$SCALA_HOME/bin:$SONAR_HOME/bin:$MONGO_HOME/bin:$IMPALA_HOME/bin:$STORM_HOME/bin:$MAHOUT_HOME/bin:$SPARK_HOME/bin:$HBASE_HOME/bin:$ACCUMULO_HOME/bin:$PIG_HOME/bin:$HIVE_HOME/bin:$ZOOKEEPER_HOME/bin:$SQOOP_HOME/bin:$HADOOP_PREFIX/bin:$MAVEN_HOME/bin:$JAVA_HOME/bin:$QUICK_CODE_HOME:/share/usr/7zip/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

export HADOOP_CLASSPATH="$HADOOP_HOME/lib:$SQOOP_HOME/lib"
export PIG_CLASSPATH="$ACCUMULO_HOME/lib/*:$PIG_CLASSPATH"

# MANAGE HADOOP
alias startyarn='$HADOOP_PREFIX/sbin/start-yarn.sh'
alias stopyarn='$HADOOP_PREFIX/sbin/stop-yarn.sh'
alias startdfs='$HADOOP_PREFIX/sbin/start-dfs.sh'
alias stopdfs='$HADOOP_PREFIX/sbin/stop-dfs.sh'
alias startdn='$HADOOP_PREFIX/bin/hdfs datanode &2>> /share/var/hadoop/datanode.out &'
alias startacc='$ACCUMULO_HOME/bin/start-all.sh'
alias stopacc='$ACCUMULO_HOME/bin/stop-all.sh'
alias accsh='accumulo shell -u $1'
alias hcatserver='$HIVE_HOME/hcatalog/sbin/hcat_server.sh $@'
alias hcatcli='$HIVE_HOME/hcatalot/bin/hcat $@'
alias hcatweb='$HIVE_HOME/hcatalog/sbin/webhcat_server.sh $@'
alias sqp='sqoop $1 --options-file $WORKSPACE/etc/sqoop.conf/sqoop-options.txt --connect jdbc:mysql://localhost/$2 '
alias startzk='$ZOOKEEPER_HOME/bin/zkServer.sh start'
alias stopzk='$ZOOKEEPER_HOME/bin/zkServer.sh stop'
alias zkcli='$ZOOKEEPER_HOME/bin/zkCli.sh -server 127.0.0.1:2181'

function hadoopstat() {
echo "hi"	
}

# getting around:
alias cdws='cd $WORKSPACE'
alias cdv='cd /share/var'
alias cddl='cd /share/downloads'
alias cdu='cd /share/usr'
alias cdm='cd $WORKSPACE/management'
alias cdh='cd $USR_DIR/hadoop'
alias cdal='cd $ACCUMULO_LOG_DIR'
alias cda='cd $ACCUMULO_HOME'
alias cdac='cd $ACCUMULO_HOME/conf'

# quickly editing things
alias vibasics='vim /share/etc/bashrc.d/basics.sh'
alias sbasics='source /share/etc/bashrc.d/basics.sh'

# other helpful methods

alias backup='$WORKSPACE/management/backup.sh $@'
alias gs='git status'
alias mci='mvn clean install'
alias mcist='mvn clean install -DskipTests'
alias setup='$WORKSPACE/management/setup.sh $@'
alias mng='$WORKSPACE/management/manager.sh $@'

alias qf='java -jar $QUICK_CODE_HOME/quickCode.jar factory true $@'
alias qb='java -jar $QUICK_CODE_HOME/quickCode.jar builder true $@'

alias rgx='java -jar /share/usr/tools/regex.jar'
alias antlr='java -jar /share/usr/antlr/antlr-4.5-complete.jar'
alias grun='java org.antlr.v4.runtime.misc.TestRig'

#  JStock commands
alias jsul='cp /share/var/workspace/knapp.open/common/iterators/target/knapp.open.common.iterators.jar /share/usr/accumulo/lib;cp /share/var/workspace/knapp.open/common/util/target/knapp.open.common.util.jar /share/usr/accumulo/lib;'
alias jsdl='java -Dspring.profiles.active=accumulo,prod -Denv=prod -Djstock.accumulo.namespace=jstock -jar /share/usr/tools/jinvestor.jstock.download.jar -y'
