
alias vi=vim
alias la='ls -lah'
alias lrot='ls -lrot'
alias ..='cd ..'
alias vl='cd /var/local'
alias w='cd /work'
alias vbrc='vim $HOME/.bashrc'
alias sbrc='source $HOME/.bashrc'
alias ldir='ls -l | egrep "^d"'
alias lf='ls -l | egrep -v "^d"'

alias ports='sudo netstat -tulpn'
alias javas='sudo ps aux | grep java'
alias jports='sudo netstat -tulpn | grep java'
alias psg='sudo ps aux | grep $@'
# alias psport='sudo netstat -tulpn | grep $1 '
alias top='top -d 6'

function portpid() {
	X=$(sudo netstat -tulpn | grep $1 | sed -r -e 's/\s+/ /g' | cut -d " " -f 7 | sed -r -e 's|/.*||g')
	echo $X
}

function psport() {
	X=$(portpid $1)
	# echo "The pid is $X"
        echo $(ps aux | grep "$X")
}
